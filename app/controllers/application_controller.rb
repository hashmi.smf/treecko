class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
  protect_from_forgery with: :null_session
  before_action :verify_for_authenticate

  protected

  def verify_for_authenticate
    unless ((controller_name.eql? 'home') && (action_name.eql? 'index')) || (controller_name.eql? 'sessions')
      authenticate_user!
    end

  end
end
