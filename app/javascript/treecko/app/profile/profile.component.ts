import { Component, OnInit } from '@angular/core'
import {Angular2TokenService} from "angular2-token";
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Router, RouterModule } from '@angular/router';


import template from './profile.component.html'

@Component({
  selector: 'profile',
  template: template
})

export class ProfileComponent implements OnInit {
  constructor(public authTokenService: Angular2TokenService, private http: HttpClient, private router: Router){}
  name = "";
  ngOnInit(): void {
    // Make the HTTP request:
    this.http.get('/posts').subscribe(
      data => {
      // Read the result field from the JSON response.
      this.name = data[0].name;
      },

      (err: HttpErrorResponse) => {
        if (err.error instanceof Error){
          console.log('client-side error') 
        }else{
          console.log('server-side error')
          this.router.navigate(['/'])
        }
        
      }
    );
  }

}