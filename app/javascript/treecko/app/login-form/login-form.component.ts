import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Router} from "@angular/router";

import {AuthService} from "../services/auth.service";
import template from './login-form.component.html'

@Component({
  selector: 'app-login-form',
  template: template
})

export class LoginFormComponent implements OnInit {
  signInUser = {email: '', password: ''};
  @Output() onFormResult = new EventEmitter<any>();
  constructor(public authService: AuthService){}
  ngOnInit() {}
  onSignInSubmit(){
    
    this.authService.logInUser(this.signInUser).subscribe(
      res => {
        if (res.status == 200){
          this.onFormResult.emit({signedIn: true, res});
          this.redirectToDashboard(res);
        }
      },

      err => {
        console.log('err', err);
        this.onFormResult.emit({signedIn: false, err});
      }
    )
  }

  redirectToDashboard(res){
    let response = res.json().data
    localStorage.setItem('id', response['id']);
    localStorage.setItem('title', response['first_post_name']);
  }
}