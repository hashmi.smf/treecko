import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {Router} from "@angular/router";

import {AuthService} from "../services/auth.service";
import template from './register-form.component.html'

@Component({
  selector: 'app-register-form',
  template: template
})

export class RegisterFormComponent implements OnInit {
  signUpUser = {email: '', password: '', passwordConfirmation: ''};
  @Output() onFormResult = new EventEmitter<any>();
  ngOnInit(){}
  constructor(private authService: AuthService){}
  
  onSignUpSubmit(){
    this.authService.registerUser(this.signUpUser).subscribe(
      (res) => {
        if(res.status == 200){
          this.onFormResult.emit({signedUp: true, res});
        }
      },
      (err) => {
        console.log('err', err);
        this.onFormResult.emit({signedUp: false, err});
      }
    )
  }
}