import { Component, OnInit, ViewChild } from '@angular/core'
import {Router} from "@angular/router";


import {AuthService} from "../services/auth.service";
import {AuthDialogComponent} from "../auth-dialog/auth-dialog.component";

import template from './toolbar.component.html'

@Component({
  selector: 'app-toolbar',
  template: template
})

export class ToolbarComponent {
  @ViewChild('authDialog') authDialog: AuthDialogComponent;

  name = 'treecko'

  constructor(public authService:AuthService, private router:Router) {}

  logOut(){
    this.authService.logOutUser().subscribe(() => this.router.navigate(['/']));
  }
 
  presentAuthDialog(mode: 'login' | 'register' = 'login'){
    this.authDialog.authDialog(mode);
  }
}