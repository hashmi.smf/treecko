import { Component, OnInit, TemplateRef, ViewChild, Input } from '@angular/core'
import { ModalDirective } from 'ngx-bootstrap/modal';
 

 import template from './auth-dialog.component.html'

@Component({
  selector: 'app-auth-dialog',
  template: template
})

export class AuthDialogComponent {
  @Input('auth-mode')authMode: 'login' | 'register' = 'login'
  @ViewChild('authModal') public authModal:ModalDirective;
 
  onLoginFormResult(e){
    if(e.signedIn)
      this.authModal.hide();
    else{
      alert(e.err.json().errors[0])
    }
  }

  onRegisterFormResult(e){
    if(e.signedUp)
      this.authModal.hide();
    else{
      alert(e.err.json().errors[0])
    }
  }
  
  authDialog(mode){
    this.authMode = mode
    this.authModal.show();
  }

  public hideChildModal():void {
    this.authModal.hide();
  }

  isLoginMode(){return this.authMode == 'login'}
  isRegisterMode(){return this.authMode == 'register'}
}