import { Component } from '@angular/core'

import template from './home.component.html'

@Component({
  selector: 'app-home',
  template: template
})

export class HomeComponent {
  name = 'treecko'
}