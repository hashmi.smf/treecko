import {Injectable} from '@angular/core'
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';  
import {Observable} from 'rxjs/observable'

@Injectable()

export class AuthInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      // Clone the request to add the new header.
      const authReq = req.clone({setHeaders: {
                                              'Content-Type': 'application/json',
                                              'access-token': localStorage.getItem('accessToken'),
                                              'client': localStorage.getItem('client'),
                                              'expiry': localStorage.getItem('expiry'),
                                              'uid': localStorage.getItem('uid'),
                                              'token-type': 'Bearer',
                                             }
                                }
                                );

      // Pass on the cloned request instead of the original request.
      return next.handle(authReq);
    }
  }