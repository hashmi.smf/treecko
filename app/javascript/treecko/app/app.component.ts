import { Component } from '@angular/core'
import {Angular2TokenService} from "angular2-token";

import template from './app.component.html'

@Component({
  selector: 'treecko',
  template: template
})

export class AppComponent {
  name = 'treecko'

  constructor(private authToken: Angular2TokenService){
    this.authToken.init({
      apiBase: 'http://localhost:5000'
    });
  

    // this.authToken.signIn({email: "learning@angular.com", password: "learning123"}).subscribe(

    //       res => {

    //         console.log('auth response:', res);
    //         console.log('auth response headers: ', res.headers.toJSON()); //log the response header to show the auth token
    //         console.log('auth response body:', res.json()); //log the response body to show the user
    //       },

    //       err => {
    //         console.error('auth error:', err);
    //       }

    // );
  }
}