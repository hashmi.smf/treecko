import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import { Angular2TokenService } from 'angular2-token';
import { ModalModule } from 'ngx-bootstrap/modal';


import { AppComponent } from './app.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { AuthDialogComponent } from './auth-dialog/auth-dialog.component';

import {AuthService} from "./services/auth.service";
import {AuthGuard} from "./guards/auth.guard";
import {AuthInterceptor} from "./auth.interceptor"
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuthDialogComponent,
    ProfileComponent,
    LoginFormComponent,
    RegisterFormComponent,
    ToolbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ModalModule.forRoot(),
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ 
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    Angular2TokenService, AuthService, AuthGuard
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
