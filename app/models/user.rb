class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable
  include DeviseTokenAuth::Concerns::User
  has_many :posts


  def first_post_name
    Post.first.name
  end
end

module DeviseTokenAuth::Concerns::User
  def token_validation_response
    self.as_json(except: [
      :tokens, :created_at, :updated_at
    ],
    methods: [
      :first_post_name
    ])
  end
end