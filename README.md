# TODO


Things needed for installation 

* Ruby 2.4.1
* Rails 5.1.2
* postgresql
* install node js
* install yarn
* refer link for above installations (http://hashmimohammed.blogspot.in/2015/06/some-ruby-on-rails.html)
* refer webpacker gem for rails and angular integration (https://github.com/rails/webpacker)

* for every package install 
** yarn add html-loader


Things you may want to cover:

* devise token based login

* order detail page

* google maps integration

* heat maps and graphs, charts

* handling million records request queries
